# Data Science R&D environment setup
Guide to setup an all-purpose work environment on a dedicated DS remote hosts.

## Hosts

You should be able to access the following hosts
```bash
dadhoc 10.21.116.22
dm 10.21.116.24
frontend 10.21.116.6
backend 10.21.116.23
```

<code>dadhoc</code> is, unsurprisingly, meant for adhoc work. <code>dm</code> stands for "Data Mining" and may be used for more established work. *ask Simon for better definitions*

## Login and SSH setup
Initially, you can ssh into these hosts using your Xaxis credentials. For the sake of convenience, we can set two things: SSH keys and a SSH configuration file. This will allow for passwordless authentication and avoid lengthy SSH commands. If this isn't needed, skip to the end of this section. 
<br><br>
For SSH Keys, execute the commands below in a Terminal.
```bash
ssh-keygen -t rsa -b 4096                             # Enter keys location when prompted, e.g. in ~/.ssh/my_ssh_key
ssh-copy-id -i ~/.ssh/my_ssh_key username@hostname    # Enter password to host when prompted
ssh-add -K ~/.ssh/my_ssh_key                          # Enter password for key when prompted
```

For SSH configuration, either edit or create the file <code>~/.ssh/config</code>, and add the following lines (e.g. for <code>dadhoc</code>):
<br>
```bash
Host dadhoc
	HostName 10.21.116.22
	User username
	UseKeychain yes
	AddKeysToAgent yes
	LocalForward XXXX localhost:XXXX    # Optional, only for remote Jupyter operation 
	IdentityFile ~/.ssh/my_ssh_key
```

Replace 'XXXX' with a number of your choice. This will later be used to locally access Jupyter sessions running on remote hosts. If remote Jupyter session is not needed, just comment out the corresponding line.<br>
You should now be able to login with <code>ssh dadhoc</code>. Note that this will also open a SSH Tunnel on port 'XXXX', which will be closed if the connection is closed (e.g. when exiting the session or closing the terminal). To open a SSH Tunnel in the background, use <code>ssh -Nf dadhoc</code>. 
<br>
<br>
**Alternatively**, if you do not want/need SSH Keys and/or configuration, you can still open a SSH tunnel with the following commands:
```bash
ssh -NfL XXXX:localhost:XXXX username@host                       # Without SSH key
ssh -i ~/.ssh/my_ssh_key -NfL XXXX:localhost:XXXX username@host  # With SSH key
```

## Python3 install
To install Python3 (Centos7 only supports Python2 natively):
```bash
sudo yum update
sudo yum install centos-release-scl
sudo yum-config-manager --enable centos-sclo-rh-testing
sudo yum install rh-python36
```
Enable Python3:
```bash
scl enable rh-python36 bash
```
Note that when exiting this shell, Python will revert to Python2. As a workaround you can enable Python inside a virtual environment, and run your Jupyter instance or scripts inside this environment.

## Remote Jupyter(Lab) session
After installing Jupyter(Lab) in your virtual environment, you can start a session and forward it to port 'XXXX' using the commands below. Note that you can start your Jupyter instance in a GNU Screen session, to prevent it from shutting down when losing connection to the host.
```bash
jupyter notebook --no-browser --port=XXXX   # Jupyter Notebook
jupyter lab --no-browser --port=XXXX        # JupyterLab
```
You can then access that session locally in your browser at URL <code>localhost:XXXX</code>.